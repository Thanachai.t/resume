## ข้อมูลทั่วไป
- ชื่อ นายธนชัย ธรรมกรณ์ (Thanachai Thammakon)
- ปีเกิด ค.ศ. 1992

## การศึกษา
1. ปวช. ช่างอิเล็กทรอนิกส์ [วิทยาลัยเทคนิคลพบุรี](http://www.lbtech.ac.th/)
1. ป.ตรี วิศวกรรมคอมพิวเตอร์ [มหาวิทยาลัยเทคโนโลยีราชมงคลธัญบุรี](https://www.rmutt.ac.th/)
<br/>

## ภาษาโปรแกรม
#### ที่มีประสบการณ์
- Javascript,&nbsp;[Nodejs](https://nodejs.org/),&nbsp; HTML,&nbsp; CSS,&nbsp;
- Java
- C#

#### ระดับเริ่มต้น
- [Typescript](https://www.typescriptlang.org/),&nbsp;
- [Flutter](https://flutter.dev/)

#### กำลังศึกษา/มีความสนใจ
- [Rust](https://www.rust-lang.org/)

<br/>

## Stack และเครื่องมือที่ใช้นในการพัฒนาเว็บ
#### Cloud
- [Google Cloud](https://cloud.google.com/),&nbsp;[Huawei Cloud](https://cloud.huawei.com/),&nbsp; [Digitalocean](https://www.digitalocean.com/),&nbsp; [Microsoft Azure](https://azure.microsoft.com/en-us/)
#### OS
- Debian,&nbsp; Ubuntu
- Alpine
#### Infrastructure
- Docker,&nbsp; Docker Compose,&nbsp; Rancher,&nbsp; Kubernetes
- Nginx

#### Database
- Mysql,&nbsp; Sequelize
- Redis
- MongoDB,&nbsp; Mongoose
- Neo4j

#### Backend
- Node.js,&nbsp; Express
- elastic.co
#### Fontend
- Vue.js,&nbsp; Vuetify,&nbsp; Nuxtjs
- Jquery,&nbsp; Bootstrap
- Sass

#### Test
- Jest,&nbsp; Jmeter,&nbsp; Mocha,&nbsp; Cypress.io

#### SVN & CI/CD
- Gitlab,&nbsp; Gitlab-runner,&nbsp; Github,&nbsp; Bitbucket
 
#### อื่น ๆ
- ติดตั่งและใช้งาน [gitlab-ce](https://gitlab.com/)
- [libp2p](https://libp2p.io/)

<br/>


## IDE และอื่น ๆ
- VS Code,&nbsp; Netbean
- [Apicur](https://www.apicur.io/),&nbsp; Postman
- lodash,&nbsp; momentjs,&nbsp; dayjs,&nbsp; gmap3
- JSP,&nbsp; JSF,&nbsp; [Primefaces](https://www.primefaces.org/)
 
<br/>

## ประวัติการทำงาน
1. 2016 ตำแหน่ง Deploy ที่ [บริษัท กลุ่มแอดวานซ์ รีเสิร์ช จำกัด](https://www.ar.co.th/home/th) 
1. 2017 - 2019 ตำแหน่ง Web Programmer ที่ [Max Savings (Thailand) Co., Ltd.](www.eofficeservice.com)
1. 2019 - 2021 ตำแหน่ง Web Programmer ที่  [บริษัท โกลบอล ไวร์เลส จำกัด](http://gitlab.globalwireless.co.th/)
 
<br/>


## Profile
Gitlab: https://gitlab.com/Thanachai.t/ \
Codestats: https://codestats.net/users/ap09248845

## ตัวอย่างผลงาน
- [somkuan-pos-web.pages.dev](https://somkuan-pos-web.pages.dev/)
- [node-web-worker](https://gitlab.com/Thanachai.t/node-web-worker.git)
- [node-config](https://public:_-Jqzy5yoFcNYrH7SeSX@www.gitlab.com/thanachai.t/node-config.git)


## ข้อมูลติดต่อ
อีเมล    [thammakon.t@gmail.com](mail:thammakon.t@gmail.com) \
โทรศัพท์ [08 7413 8592](tel:0874138592)

